﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ventas.COMUN;
using System.Configuration;
using System.Data.Common;
using System.Data;

namespace ventas.DATOS
{
    public class DatosArticulos
    {
        public DatosArticulos()
        { }

        public static string constr
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ConnectionString; }
        }

        public static string Provider //proveedor de diferentes tipos de bases de datos
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ProviderName; }
        }

        public static DbProviderFactory dpf
        {
            get { return DbProviderFactories.GetFactory(Provider); }
        }

        public static int ejecutaNonQuery(string StoredProcedure, List<DbParameter> parametros)
        {
            int Id = 0;
            try
            {
                using (DbConnection con = dpf.CreateConnection())
                {
                    con.ConnectionString = constr;

                    using (DbCommand cmd = dpf.CreateCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = StoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (DbParameter param in parametros)
                            cmd.Parameters.Add(param);
                        con.Open();
                        Id = cmd.ExecuteNonQuery();

                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                //coneccion cerrada
            }
            return Id;
        }

        public int InsertArticulo(string codigo, string nombre, string descripcion, byte[] imagen, int idcategoria, int idpresentacion  )
        {
            List<DbParameter> parametros = new List<DbParameter>(); ;

      

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = codigo;
            param1.ParameterName = "codigo";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = nombre;
            param2.ParameterName = "nombre";
            parametros.Add(param2);

            DbParameter param3 = dpf.CreateParameter();
            param3.Value = descripcion;
            param3.ParameterName = "descripcion";
            parametros.Add(param3);



            DbParameter param4 = dpf.CreateParameter();
            param4.Value = imagen;
            param4.ParameterName = "imagen";
            parametros.Add(param4);


            DbParameter param5 = dpf.CreateParameter();
            param5.Value = idcategoria;
            param5.ParameterName = "idcategoria";
            parametros.Add(param5);


            DbParameter param6 = dpf.CreateParameter();
            param6.Value = idpresentacion;
            param6.ParameterName = "idpresentacion";
            parametros.Add(param6);




            return ejecutaNonQuery("spinsertar_articulo", parametros);

            

        }

    }
}