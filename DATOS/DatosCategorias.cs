﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ventas.COMUN;
using System.Configuration;
using System.Data.Common;
using System.Data;

namespace ventas.DATOS
{
    public class DatosCategorias
    {

        public DatosCategorias()
        { }

        public static string constr
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ConnectionString; }
        }

        public static string Provider //proveedor de diferentes tipos de bases de datos
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ProviderName; }
        }

        public static DbProviderFactory dpf
        {
            get { return DbProviderFactories.GetFactory(Provider); }
        }

        public static int ejecutaNonQuery(string StoredProcedure, List<DbParameter> parametros)
        {
            int Id = 0;
            try
            {
                using (DbConnection con = dpf.CreateConnection())
                {
                    con.ConnectionString = constr;

                    using (DbCommand cmd = dpf.CreateCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = StoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (DbParameter param in parametros)
                            cmd.Parameters.Add(param);
                        con.Open();
                        Id = cmd.ExecuteNonQuery();

                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                //coneccion cerrada
            }
            return Id;
        }

        public int InsertCategoria(int idcategoria, string nombre, string descripcion)
        {
            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param = dpf.CreateParameter();
            param.Value = idcategoria;
            param.ParameterName = "idcategoria";
            parametros.Add(param);

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = nombre;
            param1.ParameterName = "nombre";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = descripcion;
            param2.ParameterName = "descripcion";
            parametros.Add(param2);

            return ejecutaNonQuery("spinsertar_categoria", parametros);

        }

        public int ActualizarCategorias(int idcategoria, string nombre, string descripcion)
        {
            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param = dpf.CreateParameter();
            param.Value = idcategoria;
            param.ParameterName = "idcategoria";
            parametros.Add(param);

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = nombre;
            param1.ParameterName = "nombre";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = descripcion;
            param2.ParameterName = "descripcion";
            parametros.Add(param2);

            return ejecutaNonQuery("speditar_categoria", parametros);
        }

        public int EliminarCategoria(int idcategoria)
        {
            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param = dpf.CreateParameter();
            param.Value = idcategoria;
            param.ParameterName = "idcategoria";
            parametros.Add(param);

            return ejecutaNonQuery("speliminar_categoria", parametros);

        }


    }
}