﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Categorias.aspx.cs" Inherits="ventas.PRESENTACION.Categorias" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1
        {
            height: 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

         <table>
             <tr>
    <td>Id</td>
    <td><asp:TextBox ID="txtid" runat="server" /> </td>
    </tr>
    <tr>
    <td>Nombre</td>
    <td><asp:TextBox ID="txtNombre" runat="server" /> </td>
    </tr>
    <tr>
    <td class="auto-style1">Descripcion</td>
    <td class="auto-style1"><asp:TextBox ID="txtDescripcion" runat="server" /> </asp:Textbox><asp:Button ID="btninsert" runat="server" Text="Agregar Categoria" onclick="btnInsert_Click" /></td>
    </tr>
    <tr>
    <td></td>
    <td>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Modificar" Width="156px" />
        </td></tr>
    </table>

          <table>
             <tr>
    <td>Seleccione el id a borrar</td>
    <td> 
        <asp:DropDownList ID="ddlidcategoria" runat="server" DataSourceID="SqlDataSource1" DataTextField="idcategoria" DataValueField="idcategoria">
        </asp:DropDownList>
                 </td>
    </tr>
    
    <td></td>
    <td>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Borrar" />
              </td></tr>
    </table>

    
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="idcategoria" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="idcategoria" HeaderText="idcategoria" InsertVisible="False" ReadOnly="True" SortExpression="idcategoria" />
                <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
                <asp:BoundField DataField="descripcion" HeaderText="descripcion" SortExpression="descripcion" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:dbventasConnectionString %>" SelectCommand="spmostrar_categoria" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </form>
</body>
</html>
