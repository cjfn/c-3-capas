﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ventas.DATOS;

namespace ventas.NEGOCIOS
{
    public class NegociosCategorias
    {
        public NegociosCategorias()
        { }

        public int IngresoCategorias(int idcategoria, string nombre, string descripcion)
        {
            DatosCategorias DatCat = new DatosCategorias();
            return DatCat.InsertCategoria(idcategoria, nombre, descripcion);
        }

        public int ActualizarCategorias(int idcategoria, string nombre, string descripcion)
        {
            DatosCategorias DatCat = new DatosCategorias();
            return DatCat.ActualizarCategorias(idcategoria, nombre, descripcion);
        }

        public int EliminarCategoria(int idcategoria)
        {
            DatosCategorias DatCat = new DatosCategorias();
            return DatCat.EliminarCategoria(idcategoria);
        }

    }
}