﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ventas.COMUN
{
    public class ComunArticulos
    {

        //constructor
        public ComunArticulos()
        { }

        //encapsular
        private int _Idarticulo;

        public int Idarticulo
        {
            get { return _Idarticulo; }
            set { _Idarticulo = value; }
        }
        private string _codigo;

        public string Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Descripcion;

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        private byte[] _Imagen;

        public byte[] Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }
        private int _Idcategoria;

        public int Idcategoria
        {
            get { return _Idcategoria; }
            set { _Idcategoria = value; }
        }
        private int _idPresentacion;

        public int IdPresentacion
        {
            get { return _idPresentacion; }
            set { _idPresentacion = value; }
        }
        private string _TextoBuscar;

        public string TextoBuscar
        {
            get { return _TextoBuscar; }
            set { _TextoBuscar = value; }
        }

        //constructor
        public ComunArticulos(int idarticulo, string codigo, string nombre, string descripcion, byte[] image, int idcategoria, int idpresentacion, string textobuscar)
        {
            this.Idarticulo = idarticulo;
            this.Codigo = codigo;
            this.Nombre = nombre;
            this.Descripcion = descripcion;
            this.Imagen = Imagen;
            this.Idcategoria = idcategoria;
            this.IdPresentacion = idpresentacion;
            this.TextoBuscar = textobuscar;
        }
    }
}