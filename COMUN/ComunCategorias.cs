﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ventas.COMUN
{
    public class ComunCategorias
    {
        public ComunCategorias()
        { }

        private int idcategoria;

        public int Idcategoria
        {
            get { return idcategoria; }
            set { idcategoria = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public ComunCategorias(int idcategoria, string nombre, string descripcion)
        {
            this.idcategoria=Idcategoria;
            this.nombre=Nombre;
            this.descripcion = Descripcion;
        }

    }
}